import React, { Component } from 'react';
import { StyleSheet, View, Text, Switch, AsyncStorage } from 'react-native';
import { Link } from 'react-router-native';

import Nav from './../components/Nav';

import { hostList, getHost, setHost } from './../utils/hostStorage';

class SettingsPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            host: getHost()
        };
    }

    onServerHostChange(host) {
        setHost(host);
        this.setState(state => Object.assign({}, state, { host: getHost() }));
    }

    render() {
        const { host } = this.state;
        return (
            <View style={styles.container}>
                <Nav>
                    <Link to="/">
                        <Text>{'<- Home'}</Text>
                    </Link>
                </Nav>
                <View style={styles.content}>
                    {hostList.map(hostItem =>
                        <View style={styles.buttonView} key={hostItem}>
                            <View style={styles.switchView}>
                                <Switch value={hostItem === host} onValueChange={() => this.onServerHostChange(hostItem)} />
                            </View>
                            <View style={styles.textView}>
                                <Text>{hostItem}</Text>
                            </View>
                        </View>
                    )}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonView: {
        padding: 10,
        flexDirection: 'row'
    },
    switchView: {
    },
    textView: {
        justifyContent: 'center',
        flex: 1,
        paddingLeft: 10
    },
    container: {
        flex: 1,
    },
    content: {
        flex: 1
    }
});

export default SettingsPage;