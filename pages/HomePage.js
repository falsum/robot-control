import React, { Component } from 'react';
import { StyleSheet, View, Text, WebView } from 'react-native';
import { Link } from 'react-router-native';

import Nav from './../components/Nav';
import Joystick from './../components/Joystick';

import { getHost } from './../utils/hostStorage';

const round = x => Math.round(x / 10) * 10;

class HomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            logItems: [],
            connected: false
        };

        this.logCounter = 0;
        this.onJoystickChange = this.onJoystickChange.bind(this);
        this.ws = new WebSocket(`ws://${getHost()}:9999`);
        this.ws.addEventListener('open', () => this.setState(state => Object.assign({}, state, {
            connected: true
        })));
        this.left = 0;
        this.right = 0;

    }

    onJoystickChange(x, y) {
        const { connected } = this.state;
        if (!connected) {
            return;
        }
        //this.addLog(`joystick: ${x} ${y}`);

        const left = round(Math.min(x, 100));
        const right = round(Math.min(y, 100));

        if (left === this.left && right === this.right) {
            return;
        }
        this.left = left;
        this.right = right;

        connected && this.ws.send(JSON.stringify({
            command: 'direction',
            l: this.left,
            r: this.right,
        }));
    }

    addLog(value) {
        this.setState(state => {
            const logItems = state.logItems.slice(-4);
            const logCounter = this.logCounter++;
            logItems.push({
                id: logCounter,
                value
            });
            return Object.assign({}, state, { logItems });
        });
    }

    componentWillUnmount() {
        this.ws.close();
    }

    formatHtml() {
        const containerStyle = 'height: 100%; margin: 0; padding: 0;';
        const bodyStyle = 'display: flex; align-items: center; justify-content: center;';
        const imgStyle = 'padding: 0; display: block; margin: 0 auto; max-height: 100%; max-width: 100%;';
        return `<html style="${containerStyle}"><body style="${containerStyle} ${bodyStyle}">
                <img style="${imgStyle}" src="http://${getHost()}:8888/stream/video.mjpeg" /></body></html>`
    }

    render() {
        const { logItems } = this.state;
        return (
            <View style={styles.container}>
                <Nav>
                    <View style={styles.navContent}>
                        <Link to="/settings">
                            <Text>Settings -></Text>
                        </Link>
                    </View>
                </Nav>
                {/* <View style={styles.console}>
                    {logItems.map(log =>
                        <Text key={log.id}>{log.value}</Text>
                    )}
                </View> */}
                <View style={styles.content}>
                    <View style={styles.videoContent}>
                        <WebView
                            style={styles.videoView}
                            automaticallyAdjustContentInsets={true}
                            scalesPageToFit={true}
                            startInLoadingState={false}
                            scrollEnabled={false}
                            source={{ html: this.formatHtml() }}
                        />
                        <View style={styles.joystickContent}>
                            <Joystick onChange={this.onJoystickChange} />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    videoView: {
        flex: 1,
    },
    videoContent: {
        flex: 1,
        padding: 20
    },
    joystickContent: {
        flex: 1,
        padding: 20
    },
    container: {
        flex: 1,
    },
    console: {
        borderBottomColor: '#aaa',
        borderBottomWidth: StyleSheet.hairlineWidth,
        alignItems: 'stretch'
    },
    content: {
        flex: 6
    },
    navContent: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});

export default HomePage;