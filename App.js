import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
    NativeRouter as Router,
    Route
} from 'react-router-native';

import HomePage from './pages/HomePage';
import SettingsPage from './pages/SettingsPage';

class App extends Component {
    render() {
        return (
            <Router>
                <View style={styles.container}>
                    <Route exact path="/" component={HomePage} />
                    <Route path="/settings" component={SettingsPage} />
                </View>
            </Router>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'stretch'
    }
});

//<Text>Open up App.js to start working on your app!22 dfgdf</Text>
//<Text>Changes you make will automatically reload.</Text>
//<Text>Shake your phone to open the developer menu.</Text>

export default App;