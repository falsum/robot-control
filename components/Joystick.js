import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    PanResponder,
    Animated
} from 'react-native';

class Joystick extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pan: new Animated.ValueXY()
        };

        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: Animated.event([null, {
                dx: this.state.pan.x,
                dy: this.state.pan.y
            }]),
            onPanResponderRelease: (e, gesture) => {
                Animated.spring(
                    this.state.pan,
                    {
                        toValue: { x: 0, y: 0 }
                    }
                ).start();
            }
        });

        this.listener = this.listener.bind(this);
    }

    listener({ x, y }) {
        const { onChange } = this.props;
        onChange(Math.trunc(x) * 1.5, Math.trunc(y) * 1.5);
    }

    componentDidMount() {
        const { pan } = this.state;
        const { onChange } = this.props;
        onChange(0, 0);
        pan.addListener(this.listener);
    }

    componentWillUnmount() {
        const { pan } = this.state;
        pan.removeAllListeners();
    }

    render() {
        const { pan } = this.state;
        const panLayout = pan.getLayout();
        return (
            <View style={styles.container}>
                <Animated.View {...this.panResponder.panHandlers} style={[panLayout, styles.stick]} />
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eee',
        justifyContent: 'center',
        alignItems: 'center',
    },
    stick: {
        width: 40,
        height: 40,
        backgroundColor: '#ed7756',
        borderColor: '#af3b1a',
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 20
    }
});

export default Joystick;