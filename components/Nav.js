import React from 'react';
import { StyleSheet, View } from 'react-native';

const Nav = props => {
    return (
        <View style={styles.container}>
            <View style={styles.content}>
                {props.children}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#eee',
        flex: 0,
        borderBottomColor: '#aaa',
        borderBottomWidth: StyleSheet.hairlineWidth,
        paddingTop: 25
    },
    content: {
        paddingTop: 10,
        paddingBottom: 10
    }
});

export default Nav;